<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email'=>$this->faker->email(),
            'password'=>$this->faker->word()
        ];
    }
}
