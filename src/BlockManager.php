<?php

namespace KDA\Filament\Blocks;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use KDA\Filament\Blocks\Blocks\BaseBlock;
use KDA\Filament\Blocks\Blocks\Placeholder;
use ReflectionClass;
use SplFileInfo;
use Str;
//use Illuminate\Support\Facades\Blade;
class BlockManager
{
    protected Collection $blocks;

    public function __construct()
    {
        $this->blocks = collect([]);
    }

    public function getBlocksByTrait(array | string | null $group = null): array
    {
        return $this->blocks
            ->when(!blank($group), function (Collection $collection) use ($group) {
                return $collection->filter(function ($item) use ($group) {
                    return in_array($group, class_uses_recursive($item));
                });
            })
            ->map(function ($block) {
                $schema = $block::getBlock()->label(fn($state) => $block::getLabel($state));
                if ($schema instanceof \KDA\Filament\CustomBuilder\CustomBlock) {
                    $schema->group($block::getGroup());
                }
                return $schema;
            })
            ->toArray();
    }

    public function getBlocksByClass(array | string | null $classes = null): array
    {
        return $this->blocks
            ->when(!blank($classes), function (Collection $collection) use ($classes) {
                return $collection->filter(function ($item) use ($classes) {
                    return in_array( $item,$classes);
                });
            })
            ->map(function ($block) {
                $schema = $block::getBlock()->label(fn($state) => $block::getLabel($state));
                if ($schema instanceof \KDA\Filament\CustomBuilder\CustomBlock) {
                    $schema->group($block::getGroup());
                }
                return $schema;
            })
            ->toArray();
    }

    public function getBlockByName(string $name): string
    {
        $class = $this->blocks->get($name);

        if (blank($class)) {
            return Placeholder::class;
        }
        return $class;
    }

    public function registerBlock(string $block): void
    {
        if (!is_subclass_of($block, BaseBlock::class)) {
            throw new \InvalidArgumentException("{$block} must extend " . BaseBlock::class);
        }

        $this->blocks->put($block::getName(), $block);
    }

    public function registerBlocks(array $blocks):void{
        foreach($blocks as $block){
            $this->registerBlock($block);
        }
    }

    public function register(string $class, string $baseClass): void
    {
        match ($baseClass) {
            BaseBlock::class => static::registerBlock($class),
            default => throw new \Exception('Invalid class type'),
        };
    }

    public function registerComponentsFromDirectory(string $baseClass, ?string $directory, ?string $namespace): void
    {
        if (blank($directory) || blank($namespace)) {
            return;
        }

        $filesystem = app(Filesystem::class);

        if (!$filesystem->exists($directory) && !Str::of($directory)->contains('*')) {
            return;
        }

        $namespace = Str::of($namespace);

        collect($filesystem->allFiles($directory))
            ->map(function (SplFileInfo $file) use ($namespace): string {
                $variableNamespace = $namespace->contains('*')
                    ? str_ireplace(
                        ['\\' . $namespace->before('*'), $namespace->after('*')],
                        ['', ''],
                        Str::of($file->getPath())
                            ->after(base_path())
                            ->replace(['/'], ['\\']),
                    )
                    : null;

                return (string) $namespace
                    ->append('\\', $file->getRelativePathname())
                    ->replace('*', $variableNamespace)
                    ->replace(['/', '.php'], ['\\', '']);
            })
            ->filter(function (string $class) use ($baseClass): bool {
                return is_subclass_of($class, $baseClass) && !(new ReflectionClass($class))->isAbstract();
            })
            ->each(fn(string $class) => $this->register($class, $baseClass))
            ->all();
    }
}
