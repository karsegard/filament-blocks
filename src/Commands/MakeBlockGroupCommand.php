<?php

namespace KDA\Filament\Blocks\Commands;

use Filament\Support\Commands\Concerns\CanManipulateFiles;
use Filament\Support\Commands\Concerns\CanValidateInput;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeBlockGroupCommand extends Command
{
    use CanManipulateFiles;
    use CanValidateInput;

    protected $signature = 'make:filament-block-group {name?} {--F|force}';

    protected $description = 'Create a new filament builder block group';

    public function handle(): int
    {
        $pageBlock = (string) Str::of($this->argument('name') ?? $this->askRequired('Name (e.g. `PostGroup`)', 'name'))
            ->trim('/')
            ->trim('\\')
            ->trim(' ')
            ->replace('/', '\\');

        $pageBlockClass = (string) Str::of($pageBlock)->afterLast('\\');

        $pageBlockNamespace = Str::of($pageBlock)->contains('\\') ?
            (string) Str::of($pageBlock)->beforeLast('\\') :
            '';

        $shortName = Str::of($pageBlock)
            ->beforeLast('Block')
            ->explode('\\')
            ->map(fn ($segment) => Str::kebab($segment))
            ->implode('.');

        
    
        $path = app_path(
            (string) Str::of($pageBlock)
                ->prepend('Filament\\Blocks\\Groups\\')
                ->replace('\\', '/')
                ->append('.php'),
        );

        $this->info ($path);
      
        $files = [$path];

        if (! $this->option('force') && $this->checkForCollision($files)) {
            return static::INVALID;
        }

        $this->copyStubToApp('Group', $path, [
            'class' => $pageBlockClass,
            'namespace' => 'App\\Filament\\Blocks\\Groups' . ($pageBlockNamespace !== '' ? "\\{$pageBlockNamespace}" : ''),
            'shortName' => $shortName,
        ]);


        $this->info("Successfully created {$pageBlock}!");

        return static::SUCCESS;
    }
}