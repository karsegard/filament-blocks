<?php

namespace KDA\Filament\Blocks;
use Filament\PluginServiceProvider;
use KDA\Filament\Blocks\Blocks\BaseBlock;
use KDA\Filament\Blocks\Facades\BlockManager;
use Spatie\LaravelPackageTools\Package;
use Livewire\Livewire;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
    ];

    protected array $relationManagers = [
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-blocks');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
       /* BlockManager::registerComponentsFromDirectory(
            BaseBlock::class,
            [],
            app_path('Filament/Blocks'),
            'App\\Filament\\Blocks'
        );*/
        //Livewire::component('filament-media-manager-modal',\KDA\Filament\MediaManager\Livewire\MediaItemModal::class);
    }
}
