<?php

namespace KDA\Filament\Blocks\Blocks;

use Filament\Forms\Components\Builder\Block;

use Closure;
use Illuminate\Contracts\Support\Htmlable;
class Placeholder extends BaseBlock
{
    protected static ?string $component = 'filament-blocks::placeholder';


    protected static  string $name = 'placeholder';


    public static function getBlockSchema(): array
    {
        return [];
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}