<?php

namespace KDA\Filament\Blocks\Blocks;


use Closure;
use Illuminate\Contracts\Support\Htmlable;
use KDA\Filament\Blocks\Blocks\Concerns\BlockClass;
use KDA\Filament\Blocks\Blocks\Concerns\EvaluatesClosure;
use KDA\Filament\Blocks\Blocks\Concerns\HasGroup;
use KDA\Filament\Blocks\Blocks\Concerns\HasLabel;
use Filament\Forms\Components\Builder\Block;
use KDA\Filament\Blocks\Blocks\Concerns\HasRelationship;
use Illuminate\View\View;

abstract class BaseBlock
{
    use EvaluatesClosure;
    use BlockClass;
    use HasLabel;
    use HasGroup;
    use HasRelationship;

    protected static ?string $component;
    protected static ?string $hint=null;
    protected static ?string $icon=null;

    protected static string $name;

    abstract public static function getBlockSchema(): array;

    public static function getName(): string
    {
        //return static::getBlock()->getName();
        return static::$name;
    }
    public static function collapsedContent(array $data): View|string
    {
        return static::$name;
    }
    public static function getBlock(): Block{
        $name =static::getName();
        $block =static::getBlockClass();
        $builderBlock = $block::make($name)->debounce()->reactive();
        $icon = static::$icon;
        $hint=  static::$hint;
        if($icon){
            $builderBlock->icon($icon);
        }
        if($hint){
            $builderBlock->hint($hint);
        }
        return $builderBlock->schema(static::getBlockSchema());
    }

    public static function getComponent(): string
    {
        if (isset(static::$component)) {
            return static::$component;
        }
        return 'filament-blocks.' . static::getName();
    }
  
    public static function mutateData(array $data): array
    {
        return $data;
    }
}