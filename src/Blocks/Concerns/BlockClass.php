<?php

namespace KDA\Filament\Blocks\Blocks\Concerns;

use Closure;
use Filament\Forms\Components\Builder\Block;

trait BlockClass
{
    protected static string $blockClass = Block::class;
    public static function getBlockClass():string{
        return static::$blockClass;
    }

    /*public static function blockClass(Closure | string $class):static{
        self::$blockClass = $class;
        return self;
    }*/
    
}
