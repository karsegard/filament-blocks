<?php

namespace KDA\Filament\Blocks\Blocks\Concerns;

use Closure;

trait EvaluatesClosure
{
    public static function evaluate($value, array $parameters = [])
    {
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }
}
