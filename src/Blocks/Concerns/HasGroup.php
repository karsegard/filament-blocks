<?php

namespace KDA\Filament\Blocks\Blocks\Concerns;

use Closure;

trait HasGroup
{
    protected static ?string $group;
    protected static bool $shouldTranslateGroup=false;

    protected static function isGroupTranslatable():bool{
        return isset(static::$shouldTranslateGroup) ? static::$shouldTranslateGroup: false;
    }

    public static function getGroup(): string | null
    {
        if (isset(static::$group)) {
            return static::isGroupTranslatable() ? __(static::$group) : static::$group;
        }
       return null;
    }
   
    
}
