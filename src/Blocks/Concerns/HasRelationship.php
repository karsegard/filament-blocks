<?php

namespace KDA\Filament\Blocks\Blocks\Concerns;

use Closure;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Model;

trait HasRelationship
{

    protected static ?string $model = null;

    public static function hasModel(): bool
    {
        return !blank(static::$model);
    }
    public static function getModel(): string
    {
        return static::$model;
    }
    public static function retrieveModel($item): Model
    {
        $data = $item['data'];
        $meta = $item['meta'] ?? null;
        ['type' => $type, 'id' => $id] = $meta;

        return (static::getModel())::find($id);
    }

    public static function createModel($item): Model{
        $data = $item['data'];
        $meta = $item['meta'] ?? null;
        return  (static::getModel())::create(['data'=>$data]);
    }
    /*public static function saveModel($data): Model{

    }

    public static function deleteModel($data): Model{

    }*/
}
