<?php

namespace KDA\Filament\Blocks\Blocks\Concerns;

use Closure;
use Illuminate\Contracts\Support\Htmlable;

trait HasLabel
{
    protected static ?string $label;
    protected static bool $shouldTranslateLabel=false;
   
 

    protected static function isLabelTranslatable():bool{
        return isset(static::$shouldTranslateLabel) ? static::$shouldTranslateLabel: false;
    }

    public static function getLabel(?array $data): string | Htmlable | Closure | null 
    {
        if (isset(static::$label)) {
            return static::isLabelTranslatable() ? __(static::$label) : static::$label;
        }
       return null;
    }
}
