<?php
namespace KDA\Filament\Blocks;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
use KDA\Filament\Blocks\Facades\BlockManager as Facade;
use KDA\Filament\Blocks\BlockManager as Library;
use KDA\Filament\Blocks\Commands\MakeBlockCommand;
use KDA\Filament\Blocks\Commands\MakeBlockGroupCommand;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasProviders;
use KDA\Laravel\Traits\HasViews;
use KDA\Filament\Blocks\Blocks\BaseBlock;
use KDA\Filament\Blocks\Facades\BlockManager;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasProviders;
    use HasViews;
    protected $packageName ='filament-blocks';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/filament-blocks.php'  => 'kda.filament-blocks'
    ];
    protected $_commands= [
        MakeBlockCommand::class,
        MakeBlockGroupCommand::class,
    ];
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\Blocks\FilamentServiceProvider::class
    ];
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
        BlockManager::registerComponentsFromDirectory(
            BaseBlock::class,
            app_path('Filament/Blocks'),
            'App\\Filament\\Blocks'
        );
    }
}
