<?php

namespace KDA\Filament\Blocks\Facades;

use Illuminate\Support\Facades\Facade;

class BlockManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
