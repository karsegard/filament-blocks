@props(['blockName','blockType'])

<div class="text-red-500 filament-block-error">
    Block with name {{$blockType}} not found.
    <br/>
    It may have been replaced
</div>