@props(['block' => []])

@php
    $b = \KDA\Filament\Blocks\Facades\BlockManager::getBlockByName($block['type']) ;
    $attributes = array_merge($block['data'],[
        'blockName'=>$b,
        'blockType'=>$block['type'],
]);
@endphp
<x-dynamic-component
    :component="$b::getComponent()"
    :attributes="new \Illuminate\View\ComponentAttributeBag($b::mutateData($attributes))"
/>