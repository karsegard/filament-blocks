# kda/filament-blocks

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/filament-blocks.svg?style=flat-square)](https://packagist.org/packages/kda/filament-blocks)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/filament-blocks.svg?style=flat-square)](https://packagist.org/packages/kda/filament-blocks)

## Reusables Blocks for Filament Builder Field

This package is a partial fork of [Z3d0X/filament-fabricator](https://github.com/Z3d0X/filament-fabricator) by [Z3d0X](https://github.com/Z3d0X)

Liked the concept but I needed to split the features in separate packages.

Works well with [kda/laravel-layouts](https://gitlab.com/karsegard/laravel-layouts) 



## Installation

You can install the package via composer:

```bash
composer require kda/filament-blocks
```


## Create a block

    php artisan make:filament-block MyBlock

This will create two files in your laravel folder 

- app/Filament/Blocks/MyBlock.php
- resources/views/components/filament-blocks/my.blade.php


## Getting the blocks

### in filament Builder form


```php 
BlockManager::getBlocks()
```
```php 
<?php


class PageResource extends Resource
{
   
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
               Builder::make('content')
                ->blocks(BlockManager::getBlocksByClass(PageGroup::class))
            ]);
    }

}
```

### in your blade template  

just call the blade component to render all the blocks

```blade
    <x-filament-blocks::blocks :blocks="$content" />
```

## rendering blocks

Your block component blade file will receive all form data as properties.

you can customize data sent to the component in the `mutateData` function of your Block
```php
    public static function mutateData(array $data): array
    {
        $data['post']= Post::find($data['post_id']);
        return $data;
    }
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
